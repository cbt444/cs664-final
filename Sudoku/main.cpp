/*
* Chris Tohline (ctohline@bu.edu)
* CS 664 Artificial Intelligence
* Final Project
*/

#include <fstream>
#include <iostream>
#include <string>

#include "Board.h"
#include "BruteForce.h"
#include "Constants.h"
#include "Generator.h"
#include "LogicSolver.h"
#include "Solver.h"

static std::ifstream* sgFile = 0;

// forward declarations
std::istream* setInputStream( int argc, char* * argv );
void readInput( std::istream* inputStream, std::string &input );
bool verifyNonEmptySquaresFromFirstBoardAreInSecondBoard(const Board& first, const Board& second);
void showUsage();

int main(int argc, char* argv[]) {
	if (argc < 2 || argc > 4) {
		showUsage();
		return -1;
	}
	if (0==strcmp(argv[1], "-generate")) {
		if (argc != 4) {
			showUsage();
			return -1;
		}
		std::cout << "Attempting to generate a new puzzle..." << std::endl;
		Generator generator(atoi(argv[2]), (0!=strcmp(argv[3], "brute")));
		generator.run();
	} else {
		std::istream* inputStream = setInputStream(argc, argv);
		if (0 == inputStream) {
			showUsage();
			return -1;
		}
		std::string input;
		readInput(inputStream, input);

		Board board;
		board.fromVegardHanssen(input);

		BruteForce bruteForceSolver(board, false);
		LogicSolver logicSolver(board, false);
		Solver* solver;

		if (0==strcmp(argv[1], "-brute")) {
			std::cout << "Solving via brute-force algorithm..." << std::endl;
			solver = &bruteForceSolver;
		} else if (0==strcmp(argv[1], "-logic")) {
			std::cout << "Solving via logic-employing algorithm..." << std::endl;
			solver = &logicSolver;
		} else if (0==strcmp(argv[1], "-both")) {
			std::cout << "Solving via logic-employing algorithm, failing over to brute force if that fails..." << std::endl;
			solver = &logicSolver;
			logicSolver.setAllowBruteFailover();
		} else {
			showUsage();
			std::cout << "====> Unknown algorithm: " << argv[1];
			return -1;
		}

		std::cout << std::endl << "Starting board:" << std::endl;
		board.print(std::cout);

		solver->solve();

		std::cout << std::endl;
		Board::Status status = solver->getBoard().gameOver();
		if (Board::YES == status) {
			if (!verifyNonEmptySquaresFromFirstBoardAreInSecondBoard(board, solver->getBoard())) {
				std::cout << "Solver changed original values on board!!!!" << std::endl;
			} else {
				std::cout << "Board state after solver is the correct solution:" << std::endl;
			}
		} else if (Board::INVALID == status) {
			std::cout << "Solver got board into an invalid configuration:" << std::endl;
		} else {
			std::cout << "Solver failed to find solution:" << std::endl;
		}
		solver->getBoard().print(std::cout);

		if (0 != sgFile) {
			sgFile->close();
			delete sgFile;
		}
	}
	return 0;
}

// side effect: may set sgFile
std::istream* setInputStream( int argc, char* * argv ) {
	switch(argc) {
	case 3: {
		// get input string from file
		char* fileName = argv[2];
		sgFile = new std::ifstream(fileName);
		std::cout << "Reading puzzle from " << fileName << "..." << std::endl;
		return sgFile;
	}
	case 2:
		// get input string from standard input
		std::cout << "Reading puzzle from standard input..." << std::endl;
		return &std::cin;
	default:
		return 0;
	}
}

void readInput( std::istream* inputStream, std::string &input ) {
	for(int linenum = 0; linenum < SUDOKU_EDGE_LENGTH; ++linenum) {
		std::string line;
		std::getline(*inputStream, line);
		input.append(line);
		input.append("\n");
	}
}

bool verifyNonEmptySquaresFromFirstBoardAreInSecondBoard(const Board& first, const Board& second) {
	for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
		for(int j=0; j<SUDOKU_EDGE_LENGTH; ++j) {
			const int firstVal = first.getPos(i,j);
			const int secondVal = second.getPos(i,j);
			if (firstVal != 0 && firstVal != secondVal) {
				return false;
			}
		}
	}
	return true;
}

void showUsage() {
	std::cerr << "Expected usage is one of:" << std::endl;
	std::cerr << "\tSudoku <algorithm> {filename}" << std::endl;
	std::cerr << "\t\tWhere <algorithm> = \"-brute\", \"-logic\", or \"-both\"" << std::endl;
	std::cerr << "\t\tAnd {filename} is optional, standard input is read if omitted" << std::endl;
	std::cerr << "Or,\tSudoku -generate <number of starting cells> <restriction>" << std::endl;
	std::cerr << "\t\tWhere <restriction> = \"brute\" or \"logic\"" << std::endl;
}
