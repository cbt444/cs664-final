/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "RowColSquareConflicts.h"

RowColSquareConflicts::RowColSquareConflicts(const Board& theBoard)
	: board(theBoard) {
}

void RowColSquareConflicts::run(CellPossibilities& possibilities) {
	for(int row=0; row<SUDOKU_EDGE_LENGTH; ++row) {
		for(int col=0; col<SUDOKU_EDGE_LENGTH; ++col) {
			if (board.getPos(row,col) == 0) {
				for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
					bool available = !board.rowContains(row,val) && !board.colContains(col,val) && !board.squareContains(board.getSquareFromRowAndCol(row,col),val);
					possibilities.setNumberAvailable(row, col, val,available);
				}
			} else {
				possibilities.setSlotFilled(row, col);
			}
		}
	}
}