/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "CellPossibilities.h"

/// find numbers that are only in a single box for a given row or column, in which case
/// other instances of those numbers in that box can be removed.
class RemoveSingleBoxPossibilities {
	const Board& board;

public:
	RemoveSingleBoxPossibilities(const Board& board);

	void run(CellPossibilities& possibilities);

private:
	// not defined
	void operator=(const RemoveSingleBoxPossibilities&);
};

