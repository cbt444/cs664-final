/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "Generator.h"

#include <algorithm>
#include <iostream>
#include <vector>

#include "Board.h"
#include "LogicSolver.h"
#include "RandomSource.h"

Generator::Generator(int theNumCells, bool theLogicOnly) 
: numCells(theNumCells)
, logicOnly(theLogicOnly) {
}

void Generator::run(void) {
	randomPlacement();
}

void Generator::randomPlacement() {
	RandomSource random;
	for(int boardNum=0; boardNum<10000000; ++boardNum) {
		Board board;
		std::vector< std::pair<int,int> > spots;
		for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
			for(int j=0; j<SUDOKU_EDGE_LENGTH; ++j) {
				spots.push_back(std::pair<int,int>(i,j));
			}
		}
		std::random_shuffle(spots.begin(), spots.end());

		// trying to create a puzzle with numCells filled
		bool placed = false;
		for(int i=0; i<numCells; ++i) {
			const std::pair<int,int>& cell = spots.at(i);
			int sqareNum = board.getSquareFromRowAndCol(cell.first, cell.second);
			placed = false;
			for(int valTry=0; valTry<15; ++valTry) {
				int val = random.getValueInRange();
				if (!board.rowContains(cell.first, val) &&
					!board.colContains(cell.second, val) &&
					!board.squareContains(sqareNum, val)) {
						board.data[cell.first][cell.second] = val;
						placed = true;
						break;
				}
			}
			if (!placed) {
				// Failed to find value for cell after 15 tries, start search over.
				break;
			}
		}

		if (placed) {
			LogicSolver logic(board, true);
			if (!logicOnly) {
				logic.setAllowBruteFailover();
			}
			logic.solve();
			Board::Status status = logic.getBoard().gameOver();
			if (Board::YES == status) {
				board.print(std::cout);
				std::cout << "Successfully solved!" << std::endl;
				logic.getBoard().print(std::cout);
				break;
			}
		}
	}
}
