/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

// obviously these constants are related!
static const int SUDOKU_EDGE_LENGTH = 9;
static const int SUDOKU_NUM_SQUARES = 9;
static const int SUDOKU_MIN_VAL = 1;
static const int SUDOKU_MAX_VAL = 9;
