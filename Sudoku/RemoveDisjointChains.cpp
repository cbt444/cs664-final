/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "RemoveDisjointChains.h"


#include <iostream>
#include <set>
#include <vector>

#include "NineBools.h"

RemoveDisjointChains::RemoveDisjointChains(const Board& theBoard)
	: board(theBoard) {
}

bool RemoveDisjointChains::run(CellPossibilities& possibilities) {
	bool changedOverall = false;
	bool changed = false;
	for(;;) {
		// std::cout << "-";
		for(int row = 0; row<SUDOKU_EDGE_LENGTH; ++row) {
			changed |= checkArea(row, row, 0, SUDOKU_EDGE_LENGTH-1, possibilities);
		}
		// std::cout << std::endl << "|";
		for(int col = 0; col<SUDOKU_EDGE_LENGTH; ++col) {
			changed |= checkArea(0, SUDOKU_EDGE_LENGTH-1, col, col, possibilities);
		}
		// std::cout << std::endl << "[]";		
		for(int square=0; square<SUDOKU_NUM_SQUARES; ++square) {
			int row = (square / 3)*3;
			int col = (square % 3)*3;
			changed |= checkArea(row,row+2,col,col+2,possibilities);
		}
		changedOverall |= changed;
		if (!changed) {
			break;
		}
		changed = false;
	}
	return changedOverall;
}

/*
 * From http://stackoverflow.com/questions/127704/algorithm-to-return-all-combinations-of-k-elements-from-n
 */
template <typename Iterator>
bool next_combination(const Iterator first, Iterator k, const Iterator last) {
	/* Credits: Mark Nelson http://marknelson.us */
	if ((first == last) || (first == k) || (last == k))
		return false;
	Iterator i1 = first;
	Iterator i2 = last;
	++i1;
	if (last == i1)
		return false;
	i1 = last;
	--i1;
	i1 = k;
	--i2;
	while (first != i1) {
		if (*--i1 < *i2) {
			Iterator j = k;
			while (!(*i1 < *j)) ++j;
			std::iter_swap(i1,j);
			++i1;
			++j;
			i2 = k;
			std::rotate(i1,j,last);
			while (last != j) {
				++j;
				++i2;
			}
			std::rotate(k,i2,last);
			return true;
		}
	}
	std::rotate(first,k,last);
	return false;
}



bool RemoveDisjointChains::checkArea(int minRow, int maxRow, int minCol, int maxCol, CellPossibilities &possibilities) {
	bool changed = false;
	for(int n=3; n<SUDOKU_EDGE_LENGTH-2; ++n) {
		std::vector< std::pair<int,int> > matchingCoordinates;
		std::vector< std::pair<int,int> > nonMatchingCoordinates;
		for(int row=minRow; row<=maxRow; ++row) {
			for(int col=minCol; col<=maxCol; ++col) {
				if ((0 == board.getPos(row,col)) && (possibilities.getAvailableCount(row,col) <= n)) {
					matchingCoordinates.push_back(std::pair<int,int>(row,col));
				} else {
					nonMatchingCoordinates.push_back(std::pair<int,int>(row,col));
				}
			}
		}
		if (static_cast<int>(matchingCoordinates.size()) <= n) {
			// give up, we don't have enough cells with possibilities, and can't get enough
			break;
		}
		do {
			bool okay = true;
			std::set<int> foundVals;
			for(int i=0; i<n && okay; ++i) {
				int row = matchingCoordinates.at(i).first;
				int col = matchingCoordinates.at(i).second;
				for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
					if (possibilities.isNumberAvailable(row,col,val)) {
						foundVals.insert(val);
						if (static_cast<int>(foundVals.size()) > n) {
							okay = false;
							break;
						}
					}
				}
			}
			if (okay && static_cast<int>(foundVals.size()) == n) {
				// found n cells that contain exactly n numbers
				// go through the other cells removing these values
				int len = nonMatchingCoordinates.size();
				for(int i=0; i<len; ++i) {
					int row = nonMatchingCoordinates.at(i).first;
					int col = nonMatchingCoordinates.at(i).second;
					for(std::set<int>::iterator itr = foundVals.begin(); itr != foundVals.end(); ++itr) {
						if (possibilities.isNumberAvailable(row,col,*itr)) {
							changed = true;
							possibilities.setNumberAvailable(row,col,*itr,false);
						}
					}
				}
				if (changed)
					break;
			}
		} while(next_combination(matchingCoordinates.begin(), matchingCoordinates.begin() + n, matchingCoordinates.end()));
	}
	return changed;
}
