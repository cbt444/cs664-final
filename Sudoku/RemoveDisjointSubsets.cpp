/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "RemoveDisjointSubsets.h"

#include <iostream>

#include "NineBools.h"

RemoveDisjointSubsets::RemoveDisjointSubsets(const Board& theBoard)
	: board(theBoard) {
}

bool RemoveDisjointSubsets::run(CellPossibilities& possibilities) {
	bool changedOverall = false;
	bool changed = false;
	for(;;) {
		// std::cout << "-";
		for(int row = 0; row<SUDOKU_EDGE_LENGTH; ++row) {
			changed |= checkArea(row, row, 0, SUDOKU_EDGE_LENGTH-1, possibilities);
		}
		// std::cout << std::endl << "|";
		for(int col = 0; col<SUDOKU_EDGE_LENGTH; ++col) {
			changed |= checkArea(0, SUDOKU_EDGE_LENGTH-1, col, col, possibilities);
		}
		// std::cout << std::endl << "[]";		
		for(int square=0; square<SUDOKU_NUM_SQUARES; ++square) {
			int row = (square / 3)*3;
			int col = (square % 3)*3;
			changed |= checkArea(row,row+2,col,col+2,possibilities);
		}
		changedOverall |= changed;
		if (!changed) {
			break;
		}
		changed = false;
	}
	return changedOverall;
}

bool RemoveDisjointSubsets::checkArea(int minRow, int maxRow, int minCol, int maxCol, CellPossibilities &possibilities) {
	bool changed = false;
	for(int len=2; len<SUDOKU_EDGE_LENGTH-1; ++len) {
		NineBools openCells(true);
		int cellNumber = 0;
		for(int row=minRow; row<=maxRow; ++row) {
			for(int col=minCol; col<=maxCol; ++col) {
				if (!openCells[cellNumber]) {
					cellNumber++;
					continue;
				}
				openCells.clear(cellNumber++);
				if ((0 == board.getPos(row,col)) && (possibilities.getAvailableCount(row,col) == len)) {
					int numMatchingLen = 1;
					NineBools matchingCells(false);
					int cellNumber2 = 0;
					for(int row2=minRow; row2<=maxRow; ++row2) {
						for(int col2=minCol; col2<=maxCol; ++col2, ++cellNumber2) {
							if (!openCells[cellNumber2]) {
								continue;
							}
							if ((0 == board.getPos(row2,col2)) && (possibilities.getAvailableCount(row2,col2) == len)) {
								bool cellHasSameAsOriginalPossibilities = true;
								for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
									if (possibilities.isNumberAvailable(row,col,val) != possibilities.isNumberAvailable(row2,col2,val)) {
										cellHasSameAsOriginalPossibilities = false;
										break;
									}
								}
								if (cellHasSameAsOriginalPossibilities) {
									matchingCells.set(cellNumber2);
									++numMatchingLen;
								}
							}
						}
					}
					if (len == numMatchingLen) {
						bool firstFirstPrint = true;
						// remove these values from the non-matching cells
						int cellNumber2 = 0;
						for(int row2=minRow; row2<=maxRow; ++row2) {
							for(int col2=minCol; col2<=maxCol; ++col2, ++cellNumber2) {
								if (matchingCells[cellNumber2] || (row == row2 && col == col2)) {
									openCells.clear(cellNumber2);
									continue;
								}
								if (0 != board.getPos(row2,col2)) {
									continue;
								}
								for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
									if (possibilities.isNumberAvailable(row,col,val)) {
										if (possibilities.isNumberAvailable(row2,col2,val)) {
											if (firstFirstPrint) {
												// std::cout << std::endl << "Matching " << row << "," << col << ":";
												firstFirstPrint = false;
											}
											// std::cout << "[X" << val << " from " << row2 << "," << col2 << "] ";
											changed = true;
										}
										possibilities.setNumberAvailable(row2,col2,val,false);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return changed;
}
