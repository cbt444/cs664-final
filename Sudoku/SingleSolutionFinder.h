/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "CellPossibilities.h"

/// check each row and column and square for cells that only have a single option
class SingleSolutionFinder {
	Board& board;
	CellPossibilities& possibilities;

public:
	SingleSolutionFinder(Board& board, CellPossibilities& possibilities);

	bool run();

private:
	// not defined
	void operator=(const SingleSolutionFinder&);
};
