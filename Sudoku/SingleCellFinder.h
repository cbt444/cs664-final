/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "CellPossibilities.h"

/// find cells that contained the only instance of a value in a row, column, or square
class SingleCellFinder {
	Board& board;
	CellPossibilities& possibilities;

public:
	SingleCellFinder(Board& board, CellPossibilities& possibilities);

	bool run();

private:
	// check area for existence of just a single value
	bool checkArea(int firstRow, int lastRow, int firstCol, int lastCol);

	// not defined
	void operator=(const SingleCellFinder&);
};