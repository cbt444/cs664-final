/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Constants.h"

class CellPossibilities {
public:
	CellPossibilities();

	void setNumberAvailable(int rowNumber, int colNumber, int val, bool available);

	void setSlotFilled(int rowNumber, int colNumber);

	bool singleNumberAvailable(int rowNumber, int colNumber, int& val);

	inline bool isNumberAvailable(int rowNumber, int colNumber, int val) {
		return number[rowNumber][colNumber][val];
	}

	inline int getAvailableCount(int rowNumber, int colNumber) const {
		int result = 0;
		for(int i=1; i<SUDOKU_MAX_VAL+1; ++i) {
			if (number[rowNumber][colNumber][i]) {
				++result;
			}
		}
		return result;
	}

	void print();

private:
	bool number[SUDOKU_EDGE_LENGTH][SUDOKU_EDGE_LENGTH][SUDOKU_MAX_VAL+1];
};
