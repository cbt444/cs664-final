/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "CellPossibilities.h"

#include <assert.h>
#include <iostream>

CellPossibilities::CellPossibilities() {
	bool initialValue = true;
	for(int row=0; row<SUDOKU_EDGE_LENGTH; ++row) {
		for(int col=0; col<SUDOKU_EDGE_LENGTH; ++col) {
			for(int slot=0; slot<SUDOKU_MAX_VAL+1; ++slot) {
				number[row][col][slot] = initialValue;
			}
		}
	}
}

void CellPossibilities::setNumberAvailable(int rowNumber, int colNumber, int val, bool available) {
	assert(0 <= val && val <= SUDOKU_MAX_VAL);
	number[rowNumber][colNumber][val] = available;
}

void CellPossibilities::setSlotFilled(int rowNumber, int colNumber) {
	for(int slot=0; slot<SUDOKU_MAX_VAL+1; ++slot) {
		number[rowNumber][colNumber][slot] = false;
	}
}

bool CellPossibilities::singleNumberAvailable(int rowNumber, int colNumber, int& val) {
	bool found = false;
	for(int slot=1; slot<SUDOKU_MAX_VAL+1; ++slot) {
		if (number[rowNumber][colNumber][slot]) {
			if (found) {
				return false;
			}
			val = slot;
			found = true;
		}
	}
	return found;
}

void CellPossibilities::print() {
	for(int row=0; row<SUDOKU_EDGE_LENGTH; ++row) {
		for(int col=0; col<SUDOKU_EDGE_LENGTH; ++col) {
			std::cout << row << "," << col << ": ";
			int spaces = 9;
			for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
				if (isNumberAvailable(row, col, val)) {
					spaces--;
					std::cout << val;
				}
			}
			while(spaces-->0) {
				std::cout << " ";
			}
			if (2 == col || 5 == col) {
				std::cout << " || ";
			}
			else if ((SUDOKU_EDGE_LENGTH-1)!=col) {
				std::cout << " | ";
			}
		}
		std::cout << std::endl;
		if (2 == row || 5 == row) {
			std::cout << "-------------------------------------------------------------------------" \
				         "-------------------------------------------------------------------------" << std::endl;
		}
	}

}