/*
* Chris Tohline (ctohline@bu.edu)
* CS 664 Artificial Intelligence
* Final Project
*/

#include "BruteForce.h"

#include <iostream>
#include "Constants.h"

BruteForce::BruteForce(const Board& otherBoard, bool inQuiet) : board(otherBoard), failCount(0), quiet(inQuiet) {
}

void BruteForce::solve() {
	solveRow(0);
	if (!quiet) {
		std::cout << std::endl << "Back-track count: " << failCount << std::endl;
	}
}

void BruteForce::addMissingElementsToPermutationGenerator(PermutationGenerator& permutations, int row) {
	for(int tryVal=1; tryVal <=SUDOKU_MAX_VAL; ++tryVal) {
		if (!board.rowContains(row, tryVal)) {
			permutations.addElement(tryVal);
		}
	}
}

bool BruteForce::solveRow(int row) {
	int originalRow[SUDOKU_EDGE_LENGTH];
	int* sourceRow = board.data[row];
	std::copy_n(sourceRow, SUDOKU_EDGE_LENGTH, originalRow);

	PermutationGenerator rowPermutations;
	addMissingElementsToPermutationGenerator(rowPermutations, row);
	int numMissingElements=rowPermutations.getLen();

	while(rowPermutations.next()) {
		int col = 0;
		bool permutationFailed = false;
		for(int eleIndex=0; !permutationFailed && eleIndex<numMissingElements; ++eleIndex) {
			while(0 != sourceRow[col]) { ++col; }
			int val = rowPermutations.currentPermutation[eleIndex];
			if (!board.colContains(col,val) && !board.squareContains(board.getSquareFromRowAndCol(row,col),val)) {
				sourceRow[col] = val;
			} else {
				// this permutation failed
				permutationFailed = true;
				failCount++;
				break;
			}
		}
		if (!permutationFailed) {
			if (row+1 < SUDOKU_EDGE_LENGTH) {
				if (solveRow(row+1)) {
					return true;
				}
			}
			if (8 == row) {
				return true;
			}
		}
		// reset for next permutation
		std::copy_n(originalRow, SUDOKU_EDGE_LENGTH, board.data[row]);
	}
	return false;;
}
