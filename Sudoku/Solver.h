/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

class Board;

class Solver {
public:
	virtual ~Solver() {}

	virtual const Board& getBoard() =0;
	virtual void solve() =0;
};
