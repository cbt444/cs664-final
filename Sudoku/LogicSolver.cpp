/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "LogicSolver.h"

#include <assert.h>
#include <iostream>
#include "Constants.h"
#include "BruteForce.h"

LogicSolver::LogicSolver(const Board& otherBoard, bool inQuiet)
	: allowBruteFailover(false)
	, quiet(inQuiet)
	, board(otherBoard)
	, singleSolutionFinder(board,possibilities)
	, singleCellFinder(board,possibilities)
	, rowColSquareConflicts(board)
	, removeSingleBoxPossibilities(board)
	, removeDisjointSubsets(board)
	, removeDisjointChains(board) {
}

void LogicSolver::solve() {
	// repeat until puzzle solved, or solver made an error and board is invalid
	while(Board::YES != board.gameOver()) {
		/// go through the entire board, updating possibilities for each cell
		rowColSquareConflicts.run(possibilities);
		for(;;) {
			removeSingleBoxPossibilities.run(possibilities);
			if (!removeDisjointSubsets.run(possibilities) && !removeDisjointChains.run(possibilities)) {
				break;
			}
		}
		// now check for new value based on solution/cell finders
		bool dirty = singleSolutionFinder.run();
		if (!dirty) {
			dirty = singleCellFinder.run();
		}

		if (!dirty) {
			if (allowBruteFailover) {
				if (!quiet) {
					std::cout << "Logic solver failed. Running brute force solver on remaining board." << std::endl;
				}
				BruteForce bruteForceSolver(board, quiet);
				bruteForceSolver.solve();
				board = bruteForceSolver.getBoard();
			} else {
				if (!quiet) {
					std::cout << "Giving up!" << std::endl;
				}
			}
			break;
		}
	}
}
