/*
* Chris Tohline (ctohline@bu.edu)
* CS 664 Artificial Intelligence
* Final Project
*/

#include "Board.h"

#include <iostream>
#include <assert.h>

Board::Board() {
	// the logic in this class doesn't work unless the MAX value is 9
	// so slap in an assert, hopefully to catch this error, were I to ever change SUDOKU_MAX_VAL
	// search for and fix all instances of '0'
	assert(SUDOKU_MAX_VAL == '9'-'0');
	for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
		for(int j=0; j<SUDOKU_EDGE_LENGTH; ++j) {
			data[i][j] = 0;
		}
	}
}

Board::Board(const Board& board) {
	for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
		for(int j=0; j<SUDOKU_EDGE_LENGTH; ++j) {
			int& val = data[i][j];
			val = board.data[i][j];
			assert(0 <= val && val <= SUDOKU_MAX_VAL);
		}
	}
}

bool Board::rowContains(int rowNumber, int val) const {
	assert(0<=rowNumber && rowNumber<SUDOKU_EDGE_LENGTH);
	for(int j=0; j<SUDOKU_EDGE_LENGTH; ++j) {
		if (val == data[rowNumber][j]) {
			return true;
		}
	}
	return false;
}

bool Board::colContains(int colNumber, int val) const {
	assert(0<=colNumber && colNumber<SUDOKU_EDGE_LENGTH);
	for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
		if (val == data[i][colNumber]) {
			return true;
		}
	}
	return false;
}

bool Board::squareContains(int squareNumber, int val) const {
	assert(0<=squareNumber && squareNumber<SUDOKU_NUM_SQUARES);
	int baseRow = (squareNumber / 3)*3;
	int baseCol = (squareNumber % 3)*3;
	for(int row=baseRow; row<baseRow+3; ++row) {
		for(int col=baseCol; col<baseCol+3; ++col) {
			if (val == data[row][col]) {
				return true;
			}
		}
	}
	return false;
}

int Board::getSquareFromRowAndCol(int rowNumber, int colNumber) const {
	return 3*(rowNumber/3)+colNumber/3;
}

/*
* The website http://www.menneske.no/sudoku/eng/index.html can be used to copy puzzles and their associated solutions.
* These are Copyright (C) Vegard Hanssen. http://www.menneske.no/eng/copyright.html So I can only use them for my
* personal development use, i.e. testing that my solver works.
*
* Returns true if successfully loaded from string.
*/
bool Board::fromVegardHanssen(const std::string& puzzleString) {
	int row = 0;
	int col = 0;
	size_t len = puzzleString.length();
	size_t i = 0;
	while (i<len) {
		char ch = puzzleString[i];
		if ('1'<=ch && ch <='9') {
			data[row][col] = ch-'0';
		} else if (' ' == ch) {
			// empty
			data[row][col] = 0;
		} else {
			std::cerr << "Failed to parse character '" << ch << "' for row " << row << ", col " << col << "! ABORTING!" << std::endl;
			return false;
		}
		// each cell is a number followed by a space and a tab
		// except for the last column
		if ((SUDOKU_EDGE_LENGTH-1) == col) {
			if (i+1 < len) {
				ch = puzzleString[i+1];
				if ('\n' != ch) {
					std::cerr << "Malformed line '" << ch << "' found instead of a newline at row " << row << ", col " << col << "! ABORTING!" << std::endl;
					return false;
				}
				i+=2;
				if ((SUDOKU_EDGE_LENGTH-1)==row) {
					// done!
					break;
				}
				col = 0;
				++row;
			} else if (row != (SUDOKU_EDGE_LENGTH-1)) {
				std::cerr << "Ran out of data at row " << row << ", col " << col << "! ABORTING!" << std::endl;
				return false;
			}
		} else {
			if (i+2 <len) {
				char ch2 = puzzleString[i+1];
				char ch3 = puzzleString[i+2];
				if (' ' != ch2 || '\t' != ch3) {
					std::cerr << "Malformed line '" << ch2 << "' and '" << ch3 << "' found instead of a space and tab at row " << row << ", col " << col << "! ABORTING!" << std::endl;
					return false;
				}
				col++;
				i+=3;
			} else {
				std::cerr << "Ran out of data for cell at row " << row << ", col " << col << "! ABORTING!" << std::endl;
				return false;
			}
		}
	}
	return true;
}

void Board::toVegardHanssen(std::string& puzzleString) const {
	for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
		for(int j=0; j<SUDOKU_EDGE_LENGTH; ++j) {
			const int& val = data[i][j];
			if (0==val) {
				puzzleString.append(1, ' ');
			} else {
				puzzleString.append(1, '0'+val);
			}
			if ((SUDOKU_EDGE_LENGTH-1)!=j) {
				puzzleString.append(" \t");
			}
		}
		puzzleString.append("\n");
	}
}

void Board::print(std::ostream& outputStream) const {
	for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
		outputStream << ' ';
		for(int j=0; j<SUDOKU_EDGE_LENGTH; ++j) {
			int val = data[i][j];
			if (0==val) {
				outputStream << ' ';
			} else {
				outputStream << static_cast<char>(('0'+val));
			}
			if (2 == j || 5 == j) {
				outputStream << " || ";
			}
			else if ((SUDOKU_EDGE_LENGTH-1)!=j) {
				outputStream << " | ";
			}
		}
		outputStream << std::endl;
		if (2 == i || 5 == i) {
			outputStream << "===|===|===||===|===|===||===|===|===" << std::endl;
		}
		else if (i != (SUDOKU_EDGE_LENGTH-1)) {
			outputStream << "---|---|---||---|---|---||---|---|---" << std::endl;
		}
	}

}

Board::Status Board::gameOver() const {
	// validate each row contains 1 through SUDOKU_MAX_VAL
	for(int row=0; row<SUDOKU_EDGE_LENGTH; ++row) {
		Status status = rowComplete(row);
		if (status != YES) {
			return status;
		}
	}
	// validate each col contains 1 through SUDOKU_MAX_VAL
	for(int col=0; col<SUDOKU_EDGE_LENGTH; ++col) {
		Status status = colComplete(col);
		if (status != YES) {
			return status;
		}
	}
	// validate each square contains 1 through SUDOKU_MAX_VAL
	for(int square=0; square<SUDOKU_NUM_SQUARES; ++square) {
		Status status = squareComplete(square);
		if (status != YES) {
			return status;
		}
	}
	return YES;
}

namespace {
	class SlotsValidator {
	public:
		SlotsValidator() {
			for(int slot=0; slot<SUDOKU_MAX_VAL+1; ++slot) {
				found[slot] = false;
			}
		}

		// return true if was already found
		bool setFound(int val) {
			assert(0 <= val && val <= SUDOKU_MAX_VAL);
			bool prev = (0 == val) ? false : found[val];
			found[val] = true;
			return prev;
		}

		Board::Status complete() {
			Board::Status result = Board::YES;
			for(int slot=1; slot<SUDOKU_MAX_VAL+1; ++slot) {
				if (!found[slot]) {
					result = Board::NO;
					break;
				}
			}
			return result;
		}

	private:
		bool found[SUDOKU_MAX_VAL+1];
	};
} // anonymous namespace

Board::Status Board::rowComplete(int row) const {
	assert(0<=row && row<SUDOKU_EDGE_LENGTH);
	SlotsValidator slotsValidator;
	for(int col=0; col<SUDOKU_EDGE_LENGTH; ++col) {
		int val = data[row][col];
		if (slotsValidator.setFound(val)) {
			return INVALID;
		}
	}
	return slotsValidator.complete();
}

Board::Status Board::colComplete(int col) const {
	assert(0<=col && col<SUDOKU_EDGE_LENGTH);
	SlotsValidator slotsValidator;
	for(int row=0; row<SUDOKU_EDGE_LENGTH; ++row) {
		int val = data[row][col];
		if (slotsValidator.setFound(val)) {
			return INVALID;
		}
	}
	return slotsValidator.complete();
}

Board::Status Board::squareComplete(int square) const {
	assert(0<=square && square<SUDOKU_NUM_SQUARES);
	SlotsValidator slotsValidator;
	int baseRow = (square / 3)*3;
	int baseCol = (square % 3)*3;
	for(int row=baseRow; row<baseRow+3; ++row) {
		for(int col=baseCol; col<baseCol+3; ++col) {
			int val = data[row][col];
			if (slotsValidator.setFound(val)) {
				return INVALID;
			}
		}
	}
	return slotsValidator.complete();
}
