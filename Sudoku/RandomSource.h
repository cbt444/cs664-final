/*
* Chris Tohline (ctohline@bu.edu)
* CS 664 Artificial Intelligence
* Final Project
*/

#pragma once
#include "Constants.h"

class RandomSource {
public:
	RandomSource();
	int get(int mod);
	inline int getValueInRange() {
		return 1+(get(SUDOKU_MAX_VAL));
	}
};
