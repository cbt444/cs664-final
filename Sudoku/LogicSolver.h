/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "CellPossibilities.h"
#include "PermutationGenerator.h"
#include "RemoveDisjointChains.h"
#include "RemoveDisjointSubsets.h"
#include "RemoveSingleBoxPossibilities.h"
#include "RowColSquareConflicts.h"
#include "SingleCellFinder.h"
#include "SingleSolutionFinder.h"
#include "Solver.h"

class LogicSolver : public Solver{
public:
	LogicSolver(const Board& board, bool quiet);
	void setAllowBruteFailover() { allowBruteFailover = true; }

	const Board& getBoard() { return board; }
	void solve();

private:
	bool allowBruteFailover;
	bool quiet;
	Board board;
	CellPossibilities possibilities;
	SingleSolutionFinder singleSolutionFinder;
	SingleCellFinder singleCellFinder;
	RowColSquareConflicts rowColSquareConflicts;
	RemoveSingleBoxPossibilities removeSingleBoxPossibilities;
	RemoveDisjointSubsets removeDisjointSubsets;
	RemoveDisjointChains removeDisjointChains;
};
