/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "PermutationGenerator.h"
#include "Solver.h"

class BruteForce : public Solver {
public:
	BruteForce(const Board& board, bool quiet);

	const Board& getBoard() { return board; }
	void solve();

private:
	void addMissingElementsToPermutationGenerator(PermutationGenerator& permutations, int row);
	bool solveRow(int row);

	Board board;
	int failCount;
	bool quiet;
};
