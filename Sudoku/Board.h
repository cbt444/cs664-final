/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include <string>
#include "Constants.h"

class Board {
public:
	int data[SUDOKU_EDGE_LENGTH][SUDOKU_EDGE_LENGTH];

	Board();
	Board(const Board& board);

	inline int& getPos(int rowNumber, int colNumber)       { return data[rowNumber][colNumber]; }
	inline int  getPos(int rowNumber, int colNumber) const { return data[rowNumber][colNumber]; }

	bool rowContains(int rowNumber, int val) const;
	bool colContains(int colNumber, int val) const;
	bool squareContains(int squareNumber, int val) const;

	int getSquareFromRowAndCol(int rowNumber, int colNumber) const;

	/// Unserialize
	bool fromVegardHanssen(const std::string& puzzleString);
	/// Serialize
	void toVegardHanssen(std::string& puzzleString) const;
	/// Pretty print
	void print(std::ostream& outputStream) const;

	enum Status {
		NO,
		YES,
		INVALID
	};
	/// Validate that entire board has been correctly filled
	Status gameOver() const;

	/// Validate row
	Status rowComplete(int rowNumber) const;
	/// Validate column
	Status colComplete(int colNumber) const;
	/// Validate square
	Status squareComplete(int squareNumber) const;
};