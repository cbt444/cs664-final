/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

class Generator
{
public:
	Generator(int numCells, bool logicOnly);

	void run();

private:
	void randomPlacement();

	int numCells;
	bool logicOnly;
};

