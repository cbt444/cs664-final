/*
* Chris Tohline (ctohline@bu.edu)
* CS 664 Artificial Intelligence
* Final Project
*/

#pragma once
#include "Constants.h"

class PermutationGenerator
{
public:
	PermutationGenerator();

	void addElement(int val);

	bool next();
	int currentPermutation[SUDOKU_EDGE_LENGTH];
	int getLen() const { return len; }
	
private:
	int len;
	bool started;
};

