/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "CellPossibilities.h"

/// Look at a row, column or box and see if you find x cells containing x numbers which must be the solution for
/// these cells, the remaining cells can't have these choices
class RemoveDisjointChains
{
	const Board& board;

public:
	RemoveDisjointChains(const Board& board);

	bool run(CellPossibilities& possibilities);


private:
	bool checkArea(int minRow, int maxRow, int minCol, int maxCol, CellPossibilities &possibilities);

	// not defined
	void operator=(const RemoveDisjointChains&);
};

