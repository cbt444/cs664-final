/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "SingleCellFinder.h"

#include <iostream>

SingleCellFinder::SingleCellFinder(Board& theBoard, CellPossibilities& thePossibilities)
	: board(theBoard), possibilities(thePossibilities) {
}

bool SingleCellFinder::run() {
	for(int row=0; row<SUDOKU_EDGE_LENGTH; ++row) {
		if (checkArea(row,row,0,SUDOKU_EDGE_LENGTH-1)) {
			return true;
		}
	}
	for(int col=0; col<SUDOKU_EDGE_LENGTH; ++col) {
		if (checkArea(0,SUDOKU_EDGE_LENGTH-1,col,col)) {
			return true;
		}
	}

	for(int square=0; square<SUDOKU_NUM_SQUARES; ++square) {
		int row = (square / 3)*3;
		int col = (square % 3)*3;
		if (checkArea(row,row+2,col,col+2)) {
			return true;
		}
	}

	return false;
}

bool SingleCellFinder::checkArea(int firstRow, int lastRow, int firstCol, int lastCol) {
	for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
		int foundCount = 0;
		int foundRow = 0;
		int foundCol = 0;
		bool foundMultiple = false;
		for(int row=firstRow; !foundMultiple && row<=lastRow; ++row) {
			for(int col=firstCol; !foundMultiple && col<=lastCol; ++col) {
				if (0 == board.getPos(row,col) && possibilities.isNumberAvailable(row, col, val)) {
					if (++foundCount > 1) {
						// this val has multiple solutions, skip it
						foundMultiple = true;
						break;
					}
					foundRow = row;
					foundCol = col;
				}
			}
		}
		if (1 == foundCount) {
			// success!
			board.getPos(foundRow,foundCol) = val;
//			std::cout << "[] (" << foundRow << "," << foundCol << ")" << std::endl;
			return true;
		}
	}
	return false;
}
