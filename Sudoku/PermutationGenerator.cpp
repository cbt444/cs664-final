/*
* Chris Tohline (ctohline@bu.edu)
* CS 664 Artificial Intelligence
* Final Project
*/

#include "PermutationGenerator.h"
#include <assert.h>
#include <algorithm>

PermutationGenerator::PermutationGenerator() : len(0), started(false) {
	std::fill_n(currentPermutation,SUDOKU_EDGE_LENGTH,0);
}

void PermutationGenerator::addElement(int val) {
	assert(len < SUDOKU_EDGE_LENGTH);
	currentPermutation[len++] = val;
	started = false;
}

bool PermutationGenerator::next() {
	if (!started) {
		started = true;
		std::sort(currentPermutation, currentPermutation+len);
		return true;
	}
	return std::next_permutation(currentPermutation, currentPermutation+len);
}
