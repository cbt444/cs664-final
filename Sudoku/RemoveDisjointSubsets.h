/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "CellPossibilities.h"

/// find subsets of n numbers, where n cells in a row/col/square are the only possibilities
/// then, the remaining cells can't have these choices
class RemoveDisjointSubsets {
	const Board& board;

public:
	RemoveDisjointSubsets(const Board& board);

	bool run(CellPossibilities& possibilities);


private:
	bool checkArea(int minRow, int maxRow, int minCol, int maxCol, CellPossibilities &possibilities);

	// not defined
	void operator=(const RemoveDisjointSubsets&);
};

