/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "RemoveSingleBoxPossibilities.h"

RemoveSingleBoxPossibilities::RemoveSingleBoxPossibilities(const Board& theBoard)
	: board(theBoard){
}

void RemoveSingleBoxPossibilities::run(CellPossibilities& possibilities) {
	// check for rows with only positions for a value in a square, then can remove value
	// from other positions in that square
	for(int row=0; row<SUDOKU_EDGE_LENGTH; ++row) {
		for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
			int foundSquare = -1;
			bool skipVal = false;
			for(int col=0; !skipVal && col<SUDOKU_EDGE_LENGTH; ++col) {
				if (0 != board.getPos(row,col)) {
					// don't check filled spots
					continue;
				}
				if (possibilities.isNumberAvailable(row,col,val)) {
					int squareForCell = board.getSquareFromRowAndCol(row,col);
					if (-1 == foundSquare) {
						foundSquare = squareForCell;
					} else if (squareForCell != foundSquare) {
						// val found in multiple squares, skip
						skipVal = true;
						break;
					}
				}
			}
			if (!skipVal && -1 != foundSquare) {
				// have to remove this value from other cells in this square
				int baseRow = (foundSquare / 3)*3;
				int baseCol = (foundSquare % 3)*3;
				for(int row2=baseRow; row2<baseRow+3; ++row2) {
					if (row2 == row) {
						continue;
					}
					for(int col2=baseCol; col2<baseCol+3; ++col2) {
						if (0 != board.getPos(row2,col2)) {
							// don't check filled spots
							continue;
						}
						if (possibilities.isNumberAvailable(row2,col2,val)) {
//							std::cout << "X " << row2 << ", " << col2 << ": " << val << std::endl;
							possibilities.setNumberAvailable(row2,col2,val,false);
						}
					}
				}
			}
		}
	}

	// check for cols with only positions for a value in a square, then can remove value
	// from other positions in that square
	for(int col=0; col<SUDOKU_EDGE_LENGTH; ++col) {
		for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
			int foundSquare = -1;
			bool skipVal = false;
			for(int row=0; !skipVal && row<SUDOKU_EDGE_LENGTH; ++row) {
				if (0 != board.getPos(row,col)) {
					// don't check filled spots
					continue;
				}
				if (possibilities.isNumberAvailable(row,col,val)) {
					int squareForCell = board.getSquareFromRowAndCol(row,col);
					if (-1 == foundSquare) {
						foundSquare = squareForCell;
					} else if (squareForCell != foundSquare) {
						// val found in multiple squares, skip
						skipVal = true;
						break;
					}
				}
			}
			if (!skipVal && -1 != foundSquare) {
				// have to remove this value from other cells in this square
				int baseRow = (foundSquare / 3)*3;
				int baseCol = (foundSquare % 3)*3;
				for(int row2=baseRow; row2<baseRow+3; ++row2) {
					for(int col2=baseCol; col2<baseCol+3; ++col2) {
						if (col2 == col) {
							continue;
						}
						if (0 != board.getPos(row2,col2)) {
							// don't check filled spots
							continue;
						}
						if (possibilities.isNumberAvailable(row2,col2,val)) {
//							std::cout << "Y " << row2 << ", " << col2 << ": " << val << std::endl;
							possibilities.setNumberAvailable(row2,col2,val,false);
						}
					}
				}
			}
		}
	}

	// check for rows/cols in each square that are the only positions for a value in that square,
	// then can remove value from other positions in that row/col
	for(int square=0; square<SUDOKU_NUM_SQUARES; ++square) {
		int baseRow = (square / 3)*3;
		int baseCol = (square % 3)*3;

		// rows
		for(int row=baseRow; row<baseRow+3; ++row) {
			for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
				bool valFoundInRow = false;
				for(int col=baseCol; col<baseCol+3; ++col) {
					if (0 != board.getPos(row,col)) {
						// don't check filled spots
						continue;
					}
					if (possibilities.isNumberAvailable(row,col,val)) {
						valFoundInRow = true;
						break;
					}
				}
				if (valFoundInRow) {
					// check other 2 rows
					bool notInOtherRows = true;
					for(int row2=baseRow; notInOtherRows && row2<baseRow+3; ++row2) {
						if (row2 == row) {
							// skip own row
							continue;
						}
						for(int col2=baseCol; notInOtherRows && col2<baseCol+3; ++col2) {
							if (0 != board.getPos(row2,col2)) {
								// don't check filled spots
								continue;
							}
							if (possibilities.isNumberAvailable(row2,col2,val)) {
								// found in multiple rows, quit
								notInOtherRows = false;
								break;
							}
						}
					}
					if (notInOtherRows) {
						// woot! only found in a single row of the square
						for(int col3=0; col3<SUDOKU_EDGE_LENGTH; ++col3) {
							if (board.getSquareFromRowAndCol(row,col3) == square) {
								// skip this square, of course
								continue;
							}
							if (0 != board.getPos(row,col3)) {
								// don't check filled spots
								continue;
							}
							if (possibilities.isNumberAvailable(row,col3,val)) {
//								std::cout << "Z1 " << row << ", " << col3 << ": " << val << std::endl;
								possibilities.setNumberAvailable(row,col3,val,false);
							}
						}
					}
				}
			}
		}

		// cols
		for(int col=baseCol; col<baseCol+3; ++col) {
			for(int val=1; val<=SUDOKU_MAX_VAL; ++val) {
				bool valFoundInCol = false;
				for(int row=baseRow; row<baseRow+3; ++row) {
					if (0 != board.getPos(row,col)) {
						// don't check filled spots
						continue;
					}
					if (possibilities.isNumberAvailable(row,col,val)) {
						valFoundInCol = true;
						break;
					}
				}
				if (valFoundInCol) {
					// check other 2 cols
					bool notInOtherCols = true;
					for(int col2=baseCol; notInOtherCols && col2<baseCol+3; ++col2) {
						if (col2 == col) {
							// skip own col
							continue;
						}
						for(int row2=baseRow; notInOtherCols && row2<baseRow+3; ++row2) {
							if (0 != board.getPos(row2,col2)) {
								// don't check filled spots
								continue;
							}
							if (possibilities.isNumberAvailable(row2,col2,val)) {
								// found in multiple cols, quit
								notInOtherCols = false;
								break;
							}
						}
					}
					if (notInOtherCols) {
						// woot! only found in a single col of the square
						for(int row3=0; row3<SUDOKU_EDGE_LENGTH; ++row3) {
							if (board.getSquareFromRowAndCol(row3, col) == square) {
								// skip this square, of course
								continue;
							}
							if (0 != board.getPos(row3,col)) {
								// don't check filled spots
								continue;
							}
							if (possibilities.isNumberAvailable(row3,col,val)) {
//								std::cout << "Z2 " << row3 << ", " << col << ": " << val << std::endl;
								possibilities.setNumberAvailable(row3,col,val,false);
							}
						}
					}
				}
			}
		}
	}
}
