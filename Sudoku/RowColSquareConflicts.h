/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include "Board.h"
#include "CellPossibilities.h"

/// Reset Possibilities to include everything except conflicts of Row, Col, or Square
class RowColSquareConflicts {
	const Board& board;

public:
	RowColSquareConflicts(const Board& board);

	void run(CellPossibilities& possibilities);

private:
	// not defined
	void operator=(const RowColSquareConflicts&);
};

