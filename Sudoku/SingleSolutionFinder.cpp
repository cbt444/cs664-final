/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#include "SingleSolutionFinder.h"


SingleSolutionFinder::SingleSolutionFinder(Board& theBoard, CellPossibilities& thePossibilities)
	: board(theBoard), possibilities(thePossibilities) {
}

bool SingleSolutionFinder::run() {
	bool solutionFound = false;
	for(int row=0; !solutionFound && row<SUDOKU_EDGE_LENGTH; ++row) {
		for(int col=0; !solutionFound && col<SUDOKU_EDGE_LENGTH; ++col) {
			int& boardPos = board.getPos(row,col);
			if (0 != boardPos) {
				continue;
			}
			int val = 0;
			if (possibilities.singleNumberAvailable(row,col,val)) {
//				std::cout << ". (" << row << "," << col << ")" << std::endl;
				boardPos = val;
				solutionFound = true;
			}
		}
	}
	return solutionFound;
}
