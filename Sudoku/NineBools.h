/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Project
 */

#pragma once

#include <assert.h>
#include "Constants.h"

class NineBools {
	bool vals[SUDOKU_EDGE_LENGTH];
public:

	NineBools(bool initialValues) {
		for(int i=0; i<SUDOKU_EDGE_LENGTH; ++i) {
			vals[i]=initialValues;
		}
	}

	inline bool operator[](int index) const {
		assert(0 <= index && index < SUDOKU_EDGE_LENGTH);
		return vals[index];
	}

	inline bool get(int index) const {
		assert(0 <= index && index < SUDOKU_EDGE_LENGTH);
		return vals[index];
	}

	inline void set(int index) {
		assert(0 <= index && index < SUDOKU_EDGE_LENGTH);
		vals[index] = true;
	}

	inline void clear(int index) {
		assert(0 <= index && index < SUDOKU_EDGE_LENGTH);
		vals[index] = false;
	}

};
