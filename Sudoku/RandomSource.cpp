/*
* Chris Tohline (ctohline@bu.edu)
* CS 664 Artificial Intelligence
* Final Project
*/

#include "RandomSource.h"
#include <stdlib.h>
#include <time.h>

RandomSource::RandomSource(void) {
	unsigned int seed = static_cast<unsigned int>(time(NULL));
	srand(seed);
}

int RandomSource::get(int mod) {
	return rand() % mod;
}
